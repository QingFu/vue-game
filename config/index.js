// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

module.exports = {
  build: {
    env: require('./prod.env'),
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '',
    productionSourceMap: false,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css']
  },
  dev: {
    env: require('./dev.env'),
    port: 8087,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    // 举例说明： 前端请求接口 /test/login.do => http://www.baidu.com/localhost/login.do
    // 这是做接口转发代理的
    proxyTable: {
      // /test 是需要转发的字符
      '/test': {
        // 转发后的地址
        target: 'http://www.baidu.com',
        changeOrigin: true,
        pathRewrite: {
          // 转发规则替换的字符
          '^/test': '/localhost'
        }
      }
    },
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}
