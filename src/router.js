import VueRouter                  from 'vue-router'
import index                      from './pages/index.vue'
import index2                      from './pages/index2.vue'

const routes = [
  /**
   * 跳转
   */
  { path: '/',      redirect: '/index' },
  { path: '/index', component: index },
  { path: '/index2', component: index2 }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})
export default router
