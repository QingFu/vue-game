import Vue           from 'vue'
import VueRouter     from 'vue-router'
import VueX          from 'vuex'
import router        from './router.js'
import Components    from './components/index.js'
import App           from './app.vue'
import store         from './store'
import axios         from 'axios'
import qs            from 'qs'

Vue.use(VueRouter)
Vue.use(VueX)
Vue.use(Components)

/**
 * axios 接口请求默认配置
 * @type {String}
 */
axios.defaults.baseURL = ''
/**
 * 设置默认请求拦截器
 */
axios.interceptors.request.use(function (config) {
  // 如果需要表单提交
  if (typeof config.dataType !== undefined) {
    if (config.dataType === 'file') {
      return config
    }
  }
  // 在请求发出之前进行一些操作
  config.data = qs.stringify(config.data)
  return config
}, function (err) {
  console.log('请求拦截器出错', err)
  return Promise.reject(err)
})
// 设置默认响应拦截器
axios.interceptors.response.use(function (response) {
  // 服务端没有推送 json 类型头，前端强制转换
  if (typeof response.data === 'string') {
    response.data = JSON.parse(response.data)
  }
  // 判断返回的状态码
  if (response.data.code === 1) {
    return response
  }
  // 请求成功
  else if (response.data.code === 200) {
    return response
  }
  else {
    Vue.prototype.$error('未知错误,错误码:' + response.data.code)
    return Promise.reject(response)
  }
}, function (error) {
  Vue.prototype.$error('接口未找到')
  return Promise.reject(error)
})

Vue.prototype.$http = axios

/**
 * 初始化
 */
/* eslint-disable no-new */
new Vue({
  store,
  router,
  el: '#app',
  render: h => h(App)
})
