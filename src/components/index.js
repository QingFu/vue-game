import Index from './components/index.vue'

export default {
  install: function (Vue) {
    Vue.component('page-index', Index)
  }
}
