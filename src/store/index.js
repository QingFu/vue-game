import Vue          from 'vue'
import Vuex         from 'vuex'
import plugins      from './plugins/plugins'
import user         from './modules/user'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    user
  },
  plugins,
  strict: debug
})
